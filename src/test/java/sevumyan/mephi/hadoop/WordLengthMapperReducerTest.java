package sevumyan.mephi.hadoop;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class WordLengthMapperReducerTest extends Assert {
    private MapDriver<Object, Text, Text, IntWritable> mapDriver;
    private ReduceDriver<Text, IntWritable, Text, IntWritable> reduceDriver;
    private MapReduceDriver<Object, Text, Text, IntWritable, Text, IntWritable> mapReduceDriver;

    @Before
    public void setUp() {
        WorldLengthMapper mapper = new WorldLengthMapper();
        WorldLengthReducer redicer = new WorldLengthReducer();
        mapDriver = MapDriver.newMapDriver(mapper);
        reduceDriver = ReduceDriver.newReduceDriver(redicer);
        mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, redicer);
    }

    @Test
    public void testMapper() throws IOException {
        mapDriver.withInput(new Text(), new Text("hadoop"));
        mapDriver.withInput(new Text(), new Text("mapreduce мапредьюс"));
        mapDriver.withInput(new Text(), new Text("hadoopfs хдфс"));
        mapDriver.withInput(new Text(), new Text("сообщество_апача apach_foundation"));
        mapDriver.withOutput(new Text("hadoop"), new IntWritable(6));
        mapDriver.withOutput(new Text("mapreduce"), new IntWritable(9));
        mapDriver.withOutput(new Text("apach_foundation"), new IntWritable(16));

        mapDriver.runTest();
    }

    @Test
    public void testReducer() throws IOException {
        List<IntWritable> list = new ArrayList<>();
        list.add(new IntWritable(1));
        reduceDriver.withInput(new Text("test"), list);
        reduceDriver.withInput(new Text("tiest"), list);
        reduceDriver.withInput(new Text("tustio"), list);
        reduceDriver.withInput(new Text("tet"), list);
        reduceDriver.withInput(new Text("tetha"), list);
        reduceDriver.withOutput(new Text("tustio"), new IntWritable(6));

        reduceDriver.runTest();
    }

    @Test
    public void testMapReducer() throws IOException {
        mapReduceDriver.withInput(new Text(), new Text("adidas найк puма"));
        mapReduceDriver.withInput(new Text(), new Text("Prileteli chaiki na bereg"));
        mapReduceDriver.withInput(new Text(), new Text("Аэродинамическое сопротивление"));
        mapReduceDriver.withInput(new Text(), new Text("Kratkost sестра таланta"));
        mapReduceDriver.withOutput(new Text("Prileteli"), new IntWritable(9));

        mapReduceDriver.runTest();
    }
}
