package sevumyan.mephi.hadoop;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

/**
 * Main class of program
 * <br/>
 * This program finds the longest word in input text and output the result to CSV file
 */
public class WordLengthParser {

    /**
     * main method of program
     *
     * @param args this program should take two string args: input file path and output file path
     */
    public static void main(String... args) throws Exception {
        Configuration conf = new Configuration();
        conf.set("mapreduce.output.textoutputformat.separator", " , ");
        Job job = Job.getInstance(conf, "word length parser");
        job.setJarByClass(WordLengthParser.class);
        job.setMapperClass(WorldLengthMapper.class);
        job.setCombinerClass(WorldLengthReducer.class);
        job.setReducerClass(WorldLengthReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}
