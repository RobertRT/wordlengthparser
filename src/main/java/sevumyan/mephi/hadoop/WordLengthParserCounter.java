package sevumyan.mephi.hadoop;

public enum WordLengthParserCounter {
    SMALL_WORDS, NON_ALPHABET_WORDS;
}
