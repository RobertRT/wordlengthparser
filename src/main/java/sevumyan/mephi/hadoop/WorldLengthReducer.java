package sevumyan.mephi.hadoop;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * Resucer class of program
 */
public class WorldLengthReducer
        extends Reducer<Text, IntWritable, Text, IntWritable> {
    private String maxWord;

    /**
     * Setup method of Reducer
     * <p>
     * This method initialise local variable, which used to find longest word
     *
     * @param context program context
     */
    @Override
    protected void setup(Context context) throws java.io.IOException, InterruptedException {
        maxWord = "";
    }

    /**
     * Reduce method of Reducer
     * <p>
     * This method finds longest word from map output
     *
     * @param key     key of the map output
     * @param value   vale of map output
     * @param context program context
     */
    @Override
    public void reduce(Text key, Iterable<IntWritable> value, Context context) throws IOException, InterruptedException {
        if (key.toString().length() > maxWord.length()) {
            maxWord = key.toString();
        }
    }

    /**
     * Cleanup method of Reducer
     * <p>
     * This method cleanups output of map state and writes the result of reduce state
     * </p>
     *
     * @param context program context
     */
    @Override
    public void cleanup(Context context) throws IOException, InterruptedException {
        context.write(new Text(maxWord), new IntWritable(maxWord.length()));
    }
}
