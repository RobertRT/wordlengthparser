package sevumyan.mephi.hadoop;

import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.StringTokenizer;

/**
 * Mapper class of program
 */
public class WorldLengthMapper extends Mapper<Object, Text, Text, IntWritable> {
    private Text word = new Text();
    private IntWritable lenght = new IntWritable(0);
    private boolean isLengthUpdated = false;

    /**
     * Map method of Mapper
     * <p>
     * This method finds longest word on line writes it to the output, if all chars is printable ascii symbols
     *
     * @param key     key of input data
     * @param value   value of input data
     * @param context program context
     */
    @Override
    public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
        StringTokenizer itr = new StringTokenizer(value.toString());
        while (itr.hasMoreTokens()) {
            String token = itr.nextToken();
            if (!StringUtils.isAlpha(token)) {
                context.getCounter(WordLengthParserCounter.NON_ALPHABET_WORDS).increment(1);
            }
            int tokenLength = token.length();
            if (tokenLength < 2) {
                context.getCounter(WordLengthParserCounter.SMALL_WORDS).increment(1);
            }
            if (StringUtils.isAsciiPrintable(token) && tokenLength > lenght.get()) {
                word.set(token);
                lenght.set(tokenLength);
                isLengthUpdated = true;
            }
        }
        if (isLengthUpdated) {
            context.write(word, new IntWritable(word.getLength()));
        }
        isLengthUpdated = false;
    }
}